Bytecodes
---------

Code | Name       | Description
-----|------------|------------
0x00 | Exit       | Exit the VM, aborting execution
0x01 | Push       | Push the specified value onto the stack
0x02 | Pop        | Pop the top value off the stack into the designated location
0x03 | Move       | Move the specified value into the specified location
0x04 | Return     | Jump to the address in the Return Point register
0x05 | Add        | Add the two top values on the stack, and then push the result
0x06 | Subtract   | Subtract the two top values on the stack, and then push the result
0x07 | Multiply   | Multiply the two top values on the stack, and then push the result
0x08 | Divide     | Divide the two top values on the stack, and then push the result
0x09 | Allocate   | Allocate the amount of memory specified in the next bytecode (in bytes) and then push the allocated address
0x10 | Free       | Free the allocated memory with the address specified in the next bytecode

Registers
---------

Name | Expanded Name        | Usage
-----|----------------------|------
IP   | Instruction Pointer  | The currently being-executed instruction
RP   | Return Point         | The instruction to return to when exiting the scope

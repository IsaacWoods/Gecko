This is a file for randomly brainstorming ideas about the Gecko language,
additions or changes or just random ideas. Feel free to add your own ideas to it
if you wish.

#### 3/06/2015 - Statically typed or dynamically typed?
`var i`
-> Infered - i is 'null'?
-> Infered - i is empty instance of 'Object'?
`var iAsAString = i.as<String>()`
-> throws RuntimeError('as' method does not exist on object of type 'null')?

Int i
-> Infered - i = 0 (inferred from default, trivial constructor)
String iAsAString = i.as<String>()
Double iAsADouble = i.as<String>()
-> throws RuntimeException(Can't assign instance of 'String' to variable of type 'Double')

It all hinges on whether Gecko is pass-by-value or pass-by-reference...

Int i = 6
var i = 6

Option A:
var i
i = 6

Option B:
Int i
i = 6

Option A: Fine if pass-by-reference, we just stick '6' somewhere in memory and adjust
`i` to point to it. In pass-by-value, we might / would have to reallocate `i`s storage
to fit in the '6'. It would be really hard to work out the type at arbitrary points.
Btw, how do we infer the type of a random var? At runtime by looking at its current
value? Mark it somehow at compile time (more bytecode per allocation)?

Option B: Fine with either. We already know that Gecko is going to be a language in
which you can't randomly mutate types, and so we know the size of a type at compile
time. In pass-by-value, we would allocate a set amount of memory when the variable
is defined, and copy data into it on allocation. In pass-by-reference, an allocation
would only change the address of the variable and each new instance would just be
allocated in memory and would have to be garbage collected (do we want Gecko to be
garbage collected?) and wouldn't be a very different system to a dynamically-typed
Gecko. In a pass-by-value system, pass-by-reference functionality could be achieved
using a object-oriented version of pointers, perhaps called `Reference`s.

#### 03/06/2015 - The Reference System
I am considering the possibility of changing Gecko to be statically-typed and the
default assigning operator actually copying the variable's contents. To still allow
the functionality of C++ pointers, but provide a more object-oriented interface and
try to limit how dangerous accessing raw memory, unlike in languages like C. The
Reference System would allow this functionality by providing the `Reference<?>`
type to wrap another type using the generic system. Various operators would be
overloaded to allow a developer to interact like `Reference`s like normal types.
If expression parsing meant that an operator with a higher precedence than method
calls, then that operator could be used to allow method calls to be easily made on
references. We could follow C and C++ and use the `->` operator to denote accessing
various things such as references (Note to Self: if we do this, make a token in the
tokenizer to make this easy to parse). We could also make it so that one can also
call a normal method with `->` as well, which would be easy with something like
(in `Object`):

```
operator ->()
{
  return this;
}
```

A rather convoluted example of the Reference System:
```
Int i = 4
Reference<Int> reference = i
String stringRepresentation = reference->as<String>()
```

#### 04/06/2015 - Generics? How the hell are they going to work?
The Reference System I just thought up requires a notion of generics. They would
also be a pretty cool thing to have in Gecko, I think. The syntax I imagine it
having would be familiar:

```
class MyGenericClass<T> {
  MyGenericClass(T handle)
  {
    // Do something
  }
}

class MyOtherGenericClass<T extends SomeClass> {

}

MyGenericClass<Int> intGenericClass = new MyGenericClass<Int>(4)
MyGenericClass<String> stringGenericClass = new MyGenericClass<String>("potato")
```

Parsing this wouldn't be too much of a challenge, but I have no idea how the code
generator would handle something like this. Since the type should always be known
at compile-time, the compiler should be able to infer what type a generic is at
any time.

Solution A:
  Expand all used types of a generic into different generated types. For example,
  a `List<Int>` becomes a `List__Int`. However, this feels like circumventing
  the problem generics try to solve.

Solution B:
  Build generics directly into a type's definition. The name of the type that
  is taking the generic's place would be needed for each variable definition, to
  allow the VM to execute methods on the generic type. Since basically everything
  in Gecko can be made into a method on a type, it is pretty easy to make just
  about anything possible with generics. All type checking would be done by the
  compiler, so the VM can be sure that all generic types are allowed at the point
  of execution.

It seems pretty obvious that Solution B is the one to go with. It might make
generics slightly more expensive to work with, but at the moment it doesn't
seem like something to worry about too much.

#### 04/06/2015 - It seems we have decided?
After thinking about generics some more, and then slowly realizing that they
would only be needed if Gecko was statically-typed, it seems we have decided,
since the generics system is something I want to see in Gecko, that the language
will be statically-typed after all. However, I am still not happy with the syntax
that literally every other language uses:

```
Int i
String myString

OR

var i : Int
var myString : String

public List<String> myList = new List<String>("Hello", ", ", "world!")
```

It also doesn't allow the clean constructor syntax I'd been hoping for:

```
MyClass() {
  member oneMember = 4
  member twoMembers = "Potato"
  member threeMembers = new List<String>()
}
```

So that needs a little experimentation, but is only a small problem. The larger
problem is that I want Gecko to be slightly less strict than C++ or Java. What does
that mean? I have no idea.

Random syntax ideas:
```
public Int myMember

MyClass(Int randomInt) {
  myMember = randomInt
}
```
-> Too much boilerplate, not nice
-> Member defined in multiple places, slightly confusing
-> Copied off Java, bit stupid looking

```
MyClass(Int randomInt) {
  String randomLocal = "Potato" // Just a local variable
  Int this.myMember = randomInt // Becomes a member because of the this.'
}
```
-> Feels like the member has been defined somewhere else, not nice feels
-> Also not pretty to define the access parameter of the member

```
MyClass(Int randomInt) {  // Member's type is inferred from parameter or local's type
  member myMember = randomInt
  public member myOtherMember = "Potato"
}
```
-> Quite pretty - basically the same syntax as before. Feels 'less strict' - nice
-> Access parameter is easy to add and not too ugly
-> Could perhaps be a little tricky to infer type of member

#### 05/06/2015 - Member syntax and other stuff
I don't think the syntax for setting members should really be that difficult,
and it would be quite easy to change in the future if we wish. At the moment,
however, I am going with an approach that is not even listed in the previous
section.

```
MyClass(Int randomInt) {
  public Int myMember = randomInt
}
```

With the addition of the access parameter making the variable a member, rather
than a local to the constructor. This means we can't have a default access parameter
setting for members, but I don't think this is a huge problem. I think this is a
little cleaner than any of the other options. It also doesn't add the complication
of trying to infer the type of the member from what's being assigned to it. With
this decision comes pretty much the final declaration that Gecko will be statically-
typed, so I'll remove the current dynamic-typing stuff from the compiler, along
with the `member` keyword.

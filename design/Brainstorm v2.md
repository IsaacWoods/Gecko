#### 19/10/2015 - New direction to Gecko?
C++ is an extremely powerful language. It is also a dangerous, bloated mess of paradigms and ideas that
has changed considerably over the years, arguably for the worst. In Gecko, I believe there to be a
language that is smaller, more concise, but rivaling in power to C++. Obviously, the development of
such a language from the ground up is no small task and one that requires considerably more talent than
I currently possess, but with time a prototype for this language may emerge. This language will be:
* Statically typed
* Compiled directly to relocatable object code
* Able to directly interface with code written in C and Assembly languages through an easier interface that C++ provides

The next step I see in developing such a language would be to build a more powerful statement and expression
parsing engine. A technique such as Pratt parsing might be an answer, but I'll have to look into it in
more depth.

#### 22/11/2015 - We need a more powerful parser debugger!
Writing the Gecko parser has become a mess. When things go wrong, it is almost impossible to tell where
the parser is within the ridiculous recursive maze that is recursive-decent parsing, even without the
Pratt-parsing that we need to add for parsing expressions. Printing the odd debug message and whatever
token we are peeking at at this point won't cut it anymore - we need a way to see exactly where we are
in the parsing of the code. Maybe it can become a novel graphical-parsing tool one can stare at whilst
their Gecko code is compiling? :P

#### 23/11/2015 - NCurses!
I have decided to write a text-based (but in a graphical sort of way) interface to see what the parser
is doing using NCurses. Never having used the aforementioned library before, this could be interesting!

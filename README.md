# Gecko
>"Within C++ is a smaller, simpler, safer language struggling to get out."
>-- <cite>Bjarne Stroustrup</cite>

Gecko is a general purpose, fast, portable programming language based on C++. It aims to be as flexible
and powerful as a language like C++, but without the platform- dependencies, quirky syntax or confusing
and verbose standard library. I started writing it as a pet project when I was 14, and it will hopefully
mature as I learn more about the internals of programming languages and compilers.

### Gecko's Aims
* Primary object-oriented paradigm, with functional programming supported
* Powerful preprocessor
* Useful yet concise standard library
* Intuitive operator overloading
* External API access, with in-built bindings for C and C++

### Build Instructions
Some GCC-only features are utilized by the Gecko compiler, but porting to another compiler shouldn't
be too difficult; in the future the compiler may be made more standard-compliant. Gecko can be built
easily on GNU Make supporting toolchains by simply running `make`. Running `make clean` will revert
the environment back to an unbuilt state.

CC = g++
CFLAGS = -g -Wall -O2 -std=c++0x -Isrc/
LFLAGS = -g -Wall -O2 -std=c++0x -Isrc/

include src/make.config
include src/parser/make.config
include src/tokenizer/make.config

.PHONY: Gecko
Gecko: $(OBJS)
	$(CC) -o $@ $^ $(LFLAGS)

%.o : %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean
clean:
	find . -type f -name '*.o' -delete
	rm -f Gecko

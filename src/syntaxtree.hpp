#ifndef SYNTAXTREE_H
#define SYNTAXTREE_H

#include <ostream>
#include <string>
#include <vector>
#include <tokenizer/tokenizer.hpp>
#include <tokenizer/tokenname.hpp>
#include <parser/symboltable.hpp>

/*
 * Base-class for nodes in the syntax tree.
 */
class TreeNode
{
  public:
    TreeNode() { }
    virtual ~TreeNode() { }

    virtual void dump(std::ostream& stream, unsigned int offset) const = 0;
};

/*
 * A command node is a common class for a node that only contains a command, but
 * does not need to provide additional parameters. They exist simply so we don't
 * need dozens of simple node definitions.
 */
class CommandNode : public TreeNode
{
  public:
    CommandNode(const std::string& command) :
      m_command(command)
    { }

    inline const std::string& getCommand() const { return m_command; }
    void dump(std::ostream& stream, unsigned int offset) const override;
  protected:
    std::string m_command;
};

/*
 * Node for referencing a variable currently in storage.
 * This does not define a variable.
 */
class VariableNode : public TreeNode
{
  public:
    VariableNode(const std::string& variableName) :
      TreeNode(),
      m_variableName(variableName)
    { }

    inline const std::string& getName() const { return m_variableName; }

    void dump(std::ostream& stream, unsigned int offset) const override;
  protected:
    std::string m_variableName;
};

/*
 * Node for calling a method with a specified name, that may or may not exist
 * in the finally loaded symbol table.
 */
class MethodNode : public TreeNode
{
  public:
    MethodNode(const std::string& name, unsigned int arity) :
      m_methodName(name),
      m_arity(arity)
    { }

    inline const std::string& getName()  const { return m_methodName;  }
    inline unsigned int getArity()       const { return m_arity;       }

    void dump(std::ostream& stream, unsigned int offset) const;
  protected:
    std::string m_methodName;
    unsigned int m_arity;
};

class PrefixNode : public TreeNode
{
  public:
    PrefixNode(TokenType operatorToken, TreeNode* right) :
      m_operator(operatorToken),
      m_right(right)
    { }

    void dump(std::ostream& stream, unsigned int offset) const;
  protected:
    TokenType m_operator;
    TreeNode* m_right;
};

class ReturnNode : public TreeNode
{
  public:
    ReturnNode(TreeNode* returnExpression) :
      m_returnExpression(returnExpression)
    { }

    void dump(std::ostream& stream, unsigned int offset) const;
  protected:
    TreeNode* m_returnExpression;
};

class IfNode : public TreeNode
{
  public:
    IfNode(TreeNode* condition, TreeNode* thenBody, TreeNode* elseBody) :
      m_condition(condition),
      m_thenBody(thenBody),
      m_elseBody(elseBody)
    { }

    void dump(std::ostream& stream, unsigned int offset) const;
  protected:
    TreeNode* m_condition;
    TreeNode* m_thenBody;
    TreeNode* m_elseBody;
};

class ForNode : public TreeNode
{
  public:
    ForNode(TreeNode* condition, TreeNode* iterationLoop) :
      m_condition(condition),
      m_iterationLoop(iterationLoop)
    { }

    void dump(std::ostream& stream, unsigned int offset) const;
  protected:
    TreeNode* m_condition;
    TreeNode* m_iterationLoop;
};

class WhileNode : public TreeNode
{
  public:
    WhileNode(TreeNode* condition, TreeNode* iterationLoop) :
      m_condition(condition),
      m_iterationLoop(iterationLoop)
    { }

    void dump(std::ostream& stream, unsigned int offset) const;
  protected:
    TreeNode* m_condition;
    TreeNode* m_iterationLoop;
};

#endif // SYNTAXTREE_H

#include <syntaxtree.hpp>

void CommandNode::dump(std::ostream& stream, unsigned int offset) const
{
  stream << std::string(offset, ' ');
  stream << "Command(\"" << m_command << "\"" << std::endl;
}

void VariableNode::dump(std::ostream& stream, unsigned int offset) const
{
  stream << std::string(offset, ' ');
  stream << "VariableRef(\"" << m_variableName << "\")" << std::endl;
}

void MethodNode::dump(std::ostream& stream, unsigned int offset) const
{
  stream << std::string(offset, ' ');
  stream << "MethodCall(\"" << m_methodName << "\")(" << m_arity << ")" << std::endl;
}

void PrefixNode::dump(std::ostream& stream, unsigned int offset) const
{
  stream << std::string(offset, ' ');

  switch (m_operator)
  {
    case TOKEN_PLUS:
      stream << "+";
      break;
    case TOKEN_MINUS:
      stream << "-";
      break;
    case TOKEN_EXCLAMATION:
      stream << "!";
      break;
    default:
      stream << "ERROR: Unexpected token passed to PrefixNode: "
        << tokenName(m_operator) << std::endl;
  }

  stream << std::string(offset, ' ');
  stream << "|" << std::endl;
  m_right->dump(stream, offset);
}

void ReturnNode::dump(std::ostream& stream, unsigned int offset) const
{
  // TODO
}

void IfNode::dump(std::ostream& stream, unsigned int offset) const
{
  // TODO
}

void ForNode::dump(std::ostream& stream, unsigned int offset) const
{
  // TODO
}

void WhileNode::dump(std::ostream& stream, unsigned int offset) const
{
  // TODO
}

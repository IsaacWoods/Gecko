#ifndef UTIL_H
#define UTIL_H

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <string.h>

namespace Util
{
  inline void pause()
  {
    std::cout << "--- PAUSE ---";
    std::cin.get();
  }

  inline std::string loadFile(const std::string& filePath)
  {
    if (!(std::ifstream(filePath)).good())
    {
      std::cerr << "Couldn't locate file: " << filePath << std::endl;
      pause();

      return NULL;
    }

    FILE* file = fopen(filePath.c_str(), "rt");
    fseek(file, 0, SEEK_END);
    unsigned int long length = ftell(file);

    char* data = new char[length + 1];
    memset(data, 0, length + 1);
    fseek(file, 0, SEEK_SET);
    unsigned int readLength = fread(data, 1, length, file);

    if (readLength != length)
    {
      std::cerr << "Failed to read complete file: " << filePath << std::endl;
      Util::pause();
    }

    std::string result(data);
    delete[] data;
    fclose(file);

    return result;
  }

  /*
   * This is a workaround for a GCC bug in which 'std::to_string' isn't correctly included
   */
  template<typename T>
  std::string toString(const T& value)
  {
    std::ostringstream stream;
    stream << value;
    return stream.str();
  }
}

#endif // UTIL_H

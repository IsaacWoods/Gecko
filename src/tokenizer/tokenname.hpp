#pragma once

#include <tokenizer/tokenizer.hpp>

const std::string& tokenName(TokenType token);

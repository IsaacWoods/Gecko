/*
 * Copyright (C) 2015, Isaac Woods. All rights reserved.
 */

#pragma once

#include <string>
#include <map>

enum TokenType
{
  // --- Symbols ---
  TOKEN_LEFT_PAREN,
  TOKEN_RIGHT_PAREN,
  TOKEN_LEFT_SQUARE_BRACE,
  TOKEN_RIGHT_SQUARE_BRACE,
  TOKEN_LEFT_BRACE,
  TOKEN_RIGHT_BRACE,
  TOKEN_COLON,
  TOKEN_DOT,
  TOKEN_COMMA,
  TOKEN_ASTERIX,
  TOKEN_SLASH,
  TOKEN_PERCENT,
  TOKEN_PLUS,
  TOKEN_MINUS,
  TOKEN_PIPE,
  TOKEN_CARET,
  TOKEN_AMPERSAND,
  TOKEN_EXCLAMATION,
  TOKEN_TILDE,
  TOKEN_QUESTION,
  TOKEN_EQUALS,
  TOKEN_LESS_THAN,
  TOKEN_GREATER_THAN,
  TOKEN_LESS_THAN_EQUAL_TO,
  TOKEN_GREATER_THAN_EQUAL_TO,
  TOKEN_DOUBLE_EQUALS,
  TOKEN_EXCLAMATION_EQUALS,

  // --- Keywords ---
  TOKEN_CLASS,
  TOKEN_METHOD,
  TOKEN_IMPORT,
  TOKEN_TRUE,
  TOKEN_FALSE,
  TOKEN_EXTENDS,
  TOKEN_NEW,
  TOKEN_RETURN,
  TOKEN_IF,
  TOKEN_ELSE,
  TOKEN_FOR,
  TOKEN_WHILE,
  TOKEN_IN,
  TOKEN_BREAK,
  TOKEN_JUMP,
  TOKEN_THIS,
  TOKEN_STATIC,
  TOKEN_VIRTUAL,
  TOKEN_NATIVE,

  // --- Identifiers ---
  TOKEN_NAME,
  TOKEN_NUMBER,
  TOKEN_STRING,

  TOKEN_INVALID,
  TOKEN_EOF
};

struct Token
{
  TokenType type;
	std::string text;
	unsigned int line, offset;

  /*
   * This should not be used!
   * It only exists to allow the STL to create S&T.
   */
  Token() :
    type(TOKEN_INVALID),
    text(""),
    line(-1)
  { }

	Token(TokenType type, unsigned int line = -1, unsigned int offset = -1, std::string text = "") :
    type(type),
    text(text),
    line(line),
    offset(offset)
  { }
};

class Tokenizer
{
  public:
    Tokenizer(const std::string& source);
    ~Tokenizer() { }

    Token Next();
    char PeekChar() const;
    char PeekNextChar() const;
    char NextChar();
  private:
    const std::string& m_source;
    Token m_previousToken, m_currentToken;
    unsigned int m_currentOffset, m_currentLine, m_lineOffset;

    #ifdef NCURSES
      bool m_isEvenToken;
    #endif

    Token ReadNumber();
    Token ReadHexNumber();
    Token ReadStringLiteral();
    Token ReadName(TokenType type);

    inline bool IsName(char c) const
    {
      return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_'));
    }

    inline bool IsDigit(char c) const
    {
      return (c >= '0' && c <= '9');
    }

    inline bool IsHexDigit(char c) const
    {
      return (IsDigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'));
    }
};

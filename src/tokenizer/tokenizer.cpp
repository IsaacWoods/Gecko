/*
 * Copyright (C) 2015, Isaac Woods. All rights reserved.
 */

#include <tokenizer/tokenizer.hpp>
#include <string>
#include <stdlib.h>
#include <util.hpp>
#include <tokenizer/tokenname.hpp>

Tokenizer::Tokenizer(const std::string& source) :
  m_source(source),
  m_currentOffset(0),
  m_currentLine(1),
  m_lineOffset(0)
{ }

Token Tokenizer::Next()
{
  m_previousToken = m_currentToken;

  if (m_previousToken.type == TOKEN_EOF)
  {
    std::cout << "WARNING: Returning TOKEN_EOF by default!" << std::endl;
    return Token(TOKEN_EOF, m_currentLine, m_lineOffset);
  }

  while (PeekChar() != '\0')
  {
    char c = NextChar();

    switch (c)
    {
      case '(': m_currentToken = Token(TOKEN_LEFT_PAREN,         m_currentLine, m_lineOffset); goto yieldToken;
      case ')': m_currentToken = Token(TOKEN_RIGHT_PAREN,        m_currentLine, m_lineOffset); goto yieldToken;
      case '[': m_currentToken = Token(TOKEN_LEFT_SQUARE_BRACE,  m_currentLine, m_lineOffset); goto yieldToken;
      case ']': m_currentToken = Token(TOKEN_RIGHT_SQUARE_BRACE, m_currentLine, m_lineOffset); goto yieldToken;
      case '{': m_currentToken = Token(TOKEN_LEFT_BRACE,         m_currentLine, m_lineOffset); goto yieldToken;
      case '}': m_currentToken = Token(TOKEN_RIGHT_BRACE,        m_currentLine, m_lineOffset); goto yieldToken;
      case ':': m_currentToken = Token(TOKEN_COLON,              m_currentLine, m_lineOffset); goto yieldToken;
      case '.': m_currentToken = Token(TOKEN_DOT,                m_currentLine, m_lineOffset); goto yieldToken;
      case ',': m_currentToken = Token(TOKEN_COMMA,              m_currentLine, m_lineOffset); goto yieldToken;
      case '*': m_currentToken = Token(TOKEN_ASTERIX,            m_currentLine, m_lineOffset); goto yieldToken;
      case '%': m_currentToken = Token(TOKEN_PERCENT,            m_currentLine, m_lineOffset); goto yieldToken;
      case '+': m_currentToken = Token(TOKEN_PLUS,               m_currentLine, m_lineOffset); goto yieldToken;
      case '-': m_currentToken = Token(TOKEN_MINUS,              m_currentLine, m_lineOffset); goto yieldToken;
      case '~': m_currentToken = Token(TOKEN_TILDE,              m_currentLine, m_lineOffset); goto yieldToken;
      case '?': m_currentToken = Token(TOKEN_QUESTION,           m_currentLine, m_lineOffset); goto yieldToken;
      case '!': m_currentToken = Token(TOKEN_EXCLAMATION,        m_currentLine, m_lineOffset); goto yieldToken;
      case '|': m_currentToken = Token(TOKEN_PIPE,               m_currentLine, m_lineOffset); goto yieldToken;
      case '&': m_currentToken = Token(TOKEN_AMPERSAND,          m_currentLine, m_lineOffset); goto yieldToken;
      case '^': m_currentToken = Token(TOKEN_CARET,              m_currentLine, m_lineOffset); goto yieldToken;
      case '=': m_currentToken = Token(TOKEN_EQUALS,             m_currentLine, m_lineOffset); goto yieldToken;
      case '<':
        if (PeekNextChar() == '=')
        {
          m_currentToken = Token(TOKEN_LESS_THAN_EQUAL_TO,       m_currentLine, m_lineOffset); goto yieldToken;
        }
        else
        {
          m_currentToken = Token(TOKEN_LESS_THAN,                m_currentLine, m_lineOffset); goto yieldToken;
        }
      case '>':
        if (PeekNextChar() == '=')
        {
          m_currentToken = Token(TOKEN_GREATER_THAN_EQUAL_TO,    m_currentLine, m_lineOffset);
          goto yieldToken;
        }
        else
        {
          m_currentToken = Token(TOKEN_GREATER_THAN,             m_currentLine, m_lineOffset);
          goto yieldToken;
        }
      case '\n':  // Fall-through intended
      case ' ':
      case '\r':
      case '\t':
        // Skip forward until there is no more whitespace
        while (PeekChar() == ' '   ||
               PeekChar() == '\r'  ||
               PeekChar() == '\t'  ||
               PeekChar() == '\n')
          NextChar();

        break;
      case '"':
        m_currentToken = ReadStringLiteral();
        goto yieldToken;
      case '_':
        break;  // TODO: somehow parse this
      case '0':
        if (PeekChar() == 'x')
        {
          m_currentToken = ReadHexNumber();
          goto yieldToken;
        }

        m_currentToken = ReadNumber();
        goto yieldToken;
      default:
        if (IsDigit(c))
        {
          m_currentToken = ReadNumber();
          goto yieldToken;
        }
        else if (IsName(c))
        {
          m_currentToken = ReadName(TOKEN_NAME);
          goto yieldToken;
        }
        else
        {
          std::cerr << "LEXICAL ERROR: Invalid character: " << c << std::endl;
          Util::pause();
        }
    }
  }

  return Token(TOKEN_INVALID, m_currentLine, m_lineOffset);

yieldToken:
  return m_currentToken;
}

char Tokenizer::PeekChar() const
{
  return m_source[m_currentOffset];
}

char Tokenizer::PeekNextChar() const
{
  if (PeekChar() == '\0')   // If we're at the end of the source code, don't read past it
    return '\0';

  return m_source[m_currentOffset + 1];
}

char Tokenizer::NextChar()
{
  char c = PeekChar();
  ++m_currentOffset;
  ++m_lineOffset;

  if (c == '\n')
  {
    ++m_currentLine;
    m_lineOffset = 0;
  }

  return c;
}

Token Tokenizer::ReadNumber()
{
  // Minus 1 to get the current char as well
  unsigned int startOffset = m_currentOffset - 1;
  while (IsDigit(PeekChar())) NextChar();

  if (PeekChar() == '.' && IsDigit(PeekNextChar()))
  {
    NextChar();
    while (IsDigit(PeekChar())) NextChar();
  }

  if (PeekChar() == 'e')
  {
    // TODO: deal with scientific notation
  }

  std::string numberStr(m_source);
  return Token(TOKEN_NUMBER, m_currentLine, m_lineOffset,
    numberStr.substr(startOffset, m_lineOffset - startOffset));
}

Token Tokenizer::ReadHexNumber()
{
  // Skip the 'x' following the '0' to mark hex notation
  NextChar();
  unsigned int startOffset = m_currentOffset;
  while (IsHexDigit(PeekChar())) NextChar();

  int number = strtol(m_source.substr(startOffset, m_lineOffset - startOffset).c_str(), NULL, 16);
  return Token(TOKEN_NUMBER, m_currentLine, m_lineOffset, Util::toString(number));
}

// Read a string literal, excluding the "" marks
Token Tokenizer::ReadStringLiteral()
{
  unsigned int startOffset = m_currentOffset;
  while (PeekChar() != '"') NextChar();
  m_currentOffset++;  // Skip the last '"' character

  return Token(TOKEN_STRING, m_currentLine, m_lineOffset,
    m_source.substr(startOffset, m_currentOffset - startOffset - 1));
}

Token Tokenizer::ReadName(TokenType type)
{
  // Minus 1 to get the current char as well
  unsigned int startOffset = m_currentOffset - 1;
  while (IsName(PeekChar())) NextChar();

  std::string nameStr = m_source.substr(startOffset, m_currentOffset - startOffset);
  unsigned int realOffset = m_lineOffset - (m_currentOffset - startOffset);

  if (nameStr == "class")    return Token(TOKEN_CLASS,    m_currentLine, realOffset);
  if (nameStr == "method")   return Token(TOKEN_METHOD,   m_currentLine, realOffset);
  if (nameStr == "import")   return Token(TOKEN_IMPORT,   m_currentLine, realOffset);
  if (nameStr == "true")     return Token(TOKEN_TRUE,     m_currentLine, realOffset);
  if (nameStr == "false")    return Token(TOKEN_FALSE,    m_currentLine, realOffset);
  if (nameStr == "extends")  return Token(TOKEN_EXTENDS,  m_currentLine, realOffset);
  if (nameStr == "new")      return Token(TOKEN_NEW,      m_currentLine, realOffset);
  if (nameStr == "return")   return Token(TOKEN_RETURN,   m_currentLine, realOffset);
  if (nameStr == "if")       return Token(TOKEN_IF,       m_currentLine, realOffset);
  if (nameStr == "else")     return Token(TOKEN_ELSE,     m_currentLine, realOffset);
  if (nameStr == "for")      return Token(TOKEN_FOR,      m_currentLine, realOffset);
  if (nameStr == "while")    return Token(TOKEN_WHILE,    m_currentLine, realOffset);
  if (nameStr == "in")       return Token(TOKEN_IN,       m_currentLine, realOffset);
  if (nameStr == "break")    return Token(TOKEN_BREAK,    m_currentLine, realOffset);
  if (nameStr == "jump")     return Token(TOKEN_JUMP,     m_currentLine, realOffset);
  if (nameStr == "this")     return Token(TOKEN_THIS,     m_currentLine, realOffset);
  if (nameStr == "static")   return Token(TOKEN_STATIC,   m_currentLine, realOffset);
  if (nameStr == "virtual")  return Token(TOKEN_VIRTUAL,  m_currentLine, realOffset);
  if (nameStr == "native")   return Token(TOKEN_NATIVE,   m_currentLine, realOffset);

  return Token(type, m_currentLine, realOffset, nameStr);
}

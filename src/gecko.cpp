/*
 * Copyright (C) 2015, Isaac Woods. All rights reserved.
 */

#include <tokenizer/tokenizer.hpp>
#include <parser/parser.hpp>
#include <util.hpp>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
#include <argp.h>
#pragma GCC diagnostic pop

#define GECKO_VERSION "0.0.1"

static void ParseArgs(int argc, char** argv)
{
  argp_program_version = (std::string("Gecko ") + GECKO_VERSION).c_str();
  error_t error;

  if ((error = argp_parse(0, argc, argv, 0, 0, 0)) != 0)
  {
    std::cerr << "Malformed invocation - try again!" << std::endl;
    exit(1);
  }
}

int main(int argc, char** argv)
{
  ParseArgs(argc, argv);

  std::string fileContents = Util::loadFile("gecko/test.gecko");
  Tokenizer tokenizer(fileContents);

  #if (0)
    Token token = tokenizer.Next();

    while (token.type != TOKEN_EOF && token.type != TOKEN_INVALID)
    {
      token = tokenizer.Next();
      std::cout << tokenName(token.type);
      if (!(token.text.empty())) std::cout << "(" << token.text << ")";
      std::cout << "{" << token.line << ":" << token.offset << "}";
      std::cout << std::endl;
    }
  #else
    Parser parser(tokenizer);
    parser.Parse();
  #endif

  return 0;
}

#include <parser/symboltable.hpp>
#include <iostream>

bool SymbolTable::isSymbolFree(const std::string& symbolName) const
{
  return (m_symbols.find(symbolName) == m_symbols.end());
}

bool SymbolTable::isSymbolFree(const std::string& symbolName, Symbol::Type symbolType) const
{
  if (m_symbols.find(symbolName) == m_symbols.end())
    return true;

  // We can't use operator[] on the map because it doesn't have a const version. *sigh*
  return (m_symbols.at(symbolName).type != symbolType);
}

void SymbolTable::insertSymbol(const std::string& symbolName, const Symbol& symbol)
{
  if (!isSymbolFree(symbolName))
    std::cout << "WARNING: Compiler tried to enter symbol that was not free: '"
      << symbolName << "'!" << std::endl;

  m_symbols.insert(std::pair<std::string, Symbol>(symbolName, symbol));
}

Symbol& SymbolTable::getSymbol(const std::string& symbolName)
{
  if (isSymbolFree(symbolName))
    std::cout << "WARNING: Compiler tried to access symbol that is not in the symbol table!"
      << std::endl;

  return m_symbols[symbolName];
}

void SymbolTable::removeSymbol(const std::string& symbolName)
{
  if (!isSymbolFree(symbolName))
  {
    std::cout << "WARNING: Compiler tried to remove a symbol that was not in the symbol table: '"
      << symbolName << "'!" << std::endl;

    return;
  }

  m_symbols.erase(symbolName);
}

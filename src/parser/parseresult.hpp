#ifndef PARSE_RESULT_H
#define PARSE_RESULT_H

#include <string>
#include <vector>
#include <syntaxtree.hpp>

enum Access
{
  PRIVATE,    // Unable to read or write from outside the class
  PROTECTED,  // Able to read but unable to write from outside the class
  PUBLIC,     // Able to both read and write from outside the class
  LOCAL       // Only accessable from the scope in which it's declared and that scope's children
};

struct MethodDefinition
{
  MethodDefinition(const std::string& methodName, Access access, bool isStatic, TreeNode* code) :
    methodName(methodName),
    access(access),
    isStatic(isStatic),
    code(code)
  { }

  std::string methodName;
  Access& access;
  bool isStatic;
  TreeNode* code;
};

struct ClassDefinition;

struct VariableDefinition
{
  VariableDefinition(const std::string& variableName, ClassDefinition* variableType, Access access) :
    variableName(variableName),
    variableType(variableType),
    access(access)
  { }

  std::string variableName;
  ClassDefinition* variableType;
  Access& access;
};

struct ClassDefinition
{
  ClassDefinition(const std::string& className) :
    className(className)
  { }

  ~ClassDefinition()
  {
    delete constructor;

    for (auto* memberVar : memberVars)
      delete memberVar;

    for (auto* method : methods)
      delete method;
  }

  std::string className;
  bool isPublic;   // If public, the class can be seen from outside the current package
  MethodDefinition* constructor;
  std::vector<VariableDefinition*> memberVars;
  std::vector<MethodDefinition*> methods;
};

struct ParseResult
{
  ParseResult() :
    hasMainMethod(false)
  { }

  std::string packageName;
  std::vector<std::string> dependencies;
  std::vector<ClassDefinition*> classes;
  bool hasMainMethod;
  MethodDefinition* mainMethod;
};

#endif // PARSE_RESULT_H

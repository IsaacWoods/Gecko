#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <map>

struct Symbol
{
  enum Type
  {
    CLASS,
    METHOD,
    VARIABLE,

    INVALID
  };

  const Type type;
  const std::string scope;

  Symbol() :
    type(INVALID),
    scope("root")
  { }

  Symbol(Type type, const std::string& scope) :
    type(type),
    scope(scope)
  { }
};

class SymbolTable
{
  public:
    SymbolTable() { }
    ~SymbolTable() { }

    bool isSymbolFree(const std::string& symbolName) const;
    bool isSymbolFree(const std::string& symbolName, Symbol::Type symbolType) const;
    void insertSymbol(const std::string& symbolName, const Symbol& symbol);
    Symbol& getSymbol(const std::string& symbolName);
    void removeSymbol(const std::string& symbolName);
  private:
    std::map<std::string, Symbol> m_symbols;
};

#endif // SYMBOLTABLE_H

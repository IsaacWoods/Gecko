#ifndef PARSELET_H
#define PARSELET_H

#include <tokenizer/tokenizer.hpp>
#include <syntaxtree.hpp>

class Parser;

class PrefixParselet
{
  public:
    PrefixParselet() { }
    virtual ~PrefixParselet() { }

    virtual TreeNode* parse(Parser& parser, const Token& token) const = 0;
};

class InfixParselet
{
  public:
    InfixParselet() { }
    virtual ~InfixParselet() { }

    virtual TreeNode* parse(Parser& parser, const Token& token, const TreeNode* left) const = 0;
};

class IdentifierParselet : public PrefixParselet
{
  public:
    IdentifierParselet() { }
    ~IdentifierParselet() { }

    TreeNode* parse(Parser& parser, const Token& token) const;
};

class PrefixOperatorParselet : public PrefixParselet
{
  public:
    PrefixOperatorParselet() : PrefixParselet() { }
    ~PrefixOperatorParselet() { }

    TreeNode* parse(Parser& parser, const Token& token) const;
};

#endif // PARSELET_H

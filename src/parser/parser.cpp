/*
 * Copyright (C) 2015, Isaac Woods. All rights reserved.
 */

#define DEBUG
#include <parser/parser.hpp>

Parser::Parser(Tokenizer& tokenizer) :
  m_tokenizer(tokenizer),
  m_result(),
  m_currentToken(tokenizer.Next()),
  m_nextToken(tokenizer.Next())
{ }

ParseResult Parser::Parse()
{
  #ifdef DEBUG
    std::cout << "--- Starting parse ---" << std::endl;
  #endif

  while (!Match(TOKEN_INVALID) && !Match(TOKEN_EOF))
  {
    if (Match(TOKEN_IMPORT)) Import();
    if (Match(TOKEN_CLASS))  Class();
    if (Match(TOKEN_METHOD)) Method();
  }

  std::cout << "--- Finished parse ---" << std::endl;
  return m_result;
}

TreeNode* Parser::Expression()
{
  LOG("->Expression")
  // TODO
  LOG("<-Expression")
  return nullptr;  // We should never reach this
}

TreeNode* Parser::Statement()
{
  LOG("->Statement")
  // Parse a variable definition
  if (Match(TOKEN_NAME) && PeekNextToken().type == TOKEN_NAME)
    return (TreeNode*) Variable();

  if (Match(TOKEN_BREAK))
  {
    NextToken();
    // TODO: make sure that we're in a loop
    LOG("Parsed break statement")
    return new CommandNode("break");
  }

  if (Match(TOKEN_RETURN))
  {
    if (!(MatchNext(TOKEN_NAME) || Match(TOKEN_NEW)))
      return nullptr;

    LOG("Parsed return statement")
    return new ReturnNode(Expression());
  }

  if (Match(TOKEN_IF))
  {
    // Compile the condition
    ConsumeNext(TOKEN_LEFT_PAREN);
    // TreeNode* condition = Expression();
    ConsumeNext(TOKEN_RIGHT_PAREN);

    TreeNode* thenBody = Block();
    TreeNode* elseBody = nullptr;

    if (MatchNext(TOKEN_ELSE))
    {
      LOG("Parsed else block")
      elseBody = Block();
    }

    LOG("Parsed if statement and block")
    return new IfNode(/*condition*/nullptr, thenBody, elseBody);
  }

  if (Match(TOKEN_FOR))
  {
    ConsumeNext(TOKEN_LEFT_PAREN);
    // TreeNode* condition = Expression();
    ConsumeNext(TOKEN_RIGHT_PAREN);

    TreeNode* iterationBody = Block();
    LOG("Parsed for loop")
    return new WhileNode(/*condition*/nullptr, iterationBody);
  }

  if (Match(TOKEN_WHILE))
  {
    ConsumeNext(TOKEN_LEFT_PAREN);
    // TreeNode* condition = parseExpression();
    ConsumeNext(TOKEN_RIGHT_PAREN);

    TreeNode* iterationBody = Block();
    LOG("Parsed while loop")
    return new WhileNode(/*condition*/nullptr, iterationBody);
  }

  LOG("<-Statement")
  return NULL;
}

void Parser::Import()
{
  LOG("->Import")

  if (!MatchNext(TOKEN_NAME))
    SYNTAX_ERROR("No package provided after 'import' keyword!")

  std::string importName;

  do
  {
    NextToken();
    importName += PeekToken().text;
    importName += '.';

    if (!Match(TOKEN_NAME))
      SYNTAX_ERROR("Malformed package name provided for import: '" << importName << '\'');

    NextToken();
  } while (Match(TOKEN_DOT));

  /*
   * Slightly grim way to strip the last '.' from the package name.
   * There's probably a better way to check if we need a '.' in the first place.
   */
  importName.pop_back();

  LOG("Importing: " << importName)
  m_result.dependencies.push_back(importName);
  LOG("<-Import")
}

void Parser::Class()
{
  LOG("->Class")

  PEEK
  PEEK_NEXT

  if (!MatchNext(TOKEN_NAME))
    SYNTAX_ERROR("A class name must be provided after the keyword 'class'!")

  std::string className = NextToken().text;

  #ifdef DEBUG
    std::cout << "Defining class: " << className << std::endl;
  #endif

  // TODO: define the class in the symbol table
  m_currentScope = className;
  ClassDefinition* definition = new ClassDefinition(className);

  ConsumeNext(TOKEN_LEFT_BRACE);

  while (!Match(TOKEN_RIGHT_BRACE))
  {
    // TODO: parse constructors, variables etc.
    if (Match(TOKEN_METHOD)) Method();
  }

  Consume(TOKEN_RIGHT_BRACE); // Consume the last '}'
  m_result.classes.push_back(definition);
  LOG("<-Class")
}

/*
 * Parse a TOKEN_NAME - the type
 * Parse a TOKEN_NAME - the name of the variable
 * (Optionally) Parse a TOKEN_EQUALS (if no TOKEN_EQAUALS -> return)
 * Parse the initial value of the variable
 */
VariableDefinition* Parser::Variable()
{
  LOG("->Variable")

  if (!MatchNext(TOKEN_NAME))
    SYNTAX_ERROR("Expected name of a type!")

  std::string variableType = PeekToken().text;

  if (!MatchNext(TOKEN_NAME))
    SYNTAX_ERROR("Invalid variable name provided!")

  std::string variableName = m_currentScope + "." + PeekToken().text;

  #ifdef DEBUG
    std::cout << "Registed variable definition: " << variableName << " of type: " << variableType << std::endl;
  #endif

  TreeNode* initExpression;
  Access accessParameter = LOCAL;

  if (!MatchNext(TOKEN_EQUALS))
    initExpression = Expression();
  else
    initExpression = nullptr; // TODO: Add nodes for call to default constructor

  // TODO: read access parameter correctly

  // TODO: correctly add code to initialise variable to method / constructor
  // TODO: access the class definition of the variable's type and add it to the definition
  // TODO: if virtual, generate an entry in the vtable for the class
  //return new VariableDefinition(variableName, NULL, accessParameter);
  LOG("<-Variable")
  return nullptr;
}

MethodDefinition* Parser::Method()
{
  LOG("->Method")

  std::string methodName = NextToken().text;   // Parse the method's name
  ConsumeNext(TOKEN_LEFT_PAREN);               // Parse the parameter list

  unsigned int arity = 0;
  std::vector<std::string> parameterNames;
  bool isStatic = false, isVirtual = false;

  if (!Match(TOKEN_RIGHT_PAREN))
  {
    if (!Match(TOKEN_NAME))
      SYNTAX_ERROR("Invalid parameter list for method: " + methodName)

    parameterNames.push_back(PeekToken().text);
    ++arity;

    while (MatchNext(TOKEN_COMMA))
    {
      if (!MatchNext(TOKEN_NAME))
        SYNTAX_ERROR("Invalid parameter list for method: " + methodName)

      parameterNames.push_back(PeekToken().text);
      ++arity;
    }
  }

  Consume(TOKEN_RIGHT_PAREN);

  if (Match(TOKEN_COLON))
  {
    NextToken();

    while (!Match(TOKEN_LEFT_BRACE))
    {
      if (Match(TOKEN_STATIC))
      {
        isStatic = true;
        NextToken();
      }

      if (Match(TOKEN_VIRTUAL))
      {
        isVirtual = true;
        NextToken();
      }
    }
  }

  std::cout << "Created a method with an arity of: " << arity << " which is: " <<
               (isStatic ? "static" : "not static") << " and " << (isVirtual ? "virtual" : "not virtual") << std::endl;

  TreeNode* methodCode = Block();

  // TODO: parse access parameter and the 'static' keyword correctly
  // return new MethodDefinition(methodName, PUBLIC, isStatic, methodCode);
  LOG("<-Method")
  return nullptr;
}

TreeNode* Parser::Block()
{
  LOG("->Block")
  Consume(TOKEN_LEFT_BRACE);

  // TODO: why the hell can't we have empty blocks!?!?! Goddamn it past-self.
  do
  {
    TreeNode* statementNode = Statement();
    delete statementNode;
  } while (!Match(TOKEN_RIGHT_BRACE));

  Consume(TOKEN_RIGHT_BRACE);
  LOG("<-Block")
  return NULL; // TODO: actually return a node for the block
}

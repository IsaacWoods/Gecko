/*
 * Copyright (C) 2015, Isaac Woods. All rights reserved.
 */

#pragma once

#include <string>
#include <iostream>
#include <tokenizer/tokenizer.hpp>
#include <parser/parseresult.hpp>

#define SYNTAX_ERROR(message) { \
  std::cerr << "SYNTAX ERROR(" << PeekToken().line << ":" << PeekToken().offset << ") " << message \
  << std::endl; exit(1); }

#ifdef DEBUG
  #define PEEK std::cout<<"PeekToken: "<<tokenName(PeekToken().type)<<std::endl;
  #define PEEK_NEXT std::cout<<"NextPeek: "<<tokenName(PeekNextToken().type)<<std::endl;
  #define LOG(message) std::cout<<message<<std::endl;
#else
  #define PEEK
  #define PEEK_NEXT
  #define LOG(message)
#endif

class Parser
{
public:
  Parser(Tokenizer& tokenizer);
  ~Parser() { }

  ParseResult Parse();
private:
  Tokenizer&   m_tokenizer;
	ParseResult  m_result;
  Token        m_currentToken;
  Token        m_nextToken;
  std::string  m_currentScope;

  // --- Methods for parsing bits of source in recursive decent ---
  TreeNode* Expression();
  TreeNode* Statement();
  void Import();
  void Class();
  VariableDefinition* Variable();
  MethodDefinition* Method();
  // void parseMainMethod();
  TreeNode* Block();

  void Consume(TokenType tokenType)
  {
    if (!Match(tokenType))
      SYNTAX_ERROR("Expected '" + tokenName(tokenType) + "', but got '" + tokenName(m_currentToken.type) + "'")

    NextToken();
  }

  void ConsumeNext(TokenType tokenType)
  {
    TokenType nextTokenType = NextToken().type;

    if (nextTokenType != tokenType)
      SYNTAX_ERROR("Expected '" + tokenName(tokenType) + "', but got '" + tokenName(nextTokenType) + "'")

    NextToken();
  }

  void ConsumeIfAny(TokenType tokenType)
  {
    if (PeekToken().type == tokenType)
        NextToken();

    while (PeekNextToken().type == tokenType)
      NextToken();
  }

  Token NextToken()
  {
    m_currentToken = m_nextToken;
    m_nextToken = m_tokenizer.Next();

    std::cout << "Peek: " << tokenName(m_currentToken.type) << std::endl;
    return m_currentToken;
  }

	inline Token PeekToken() const
	{
    std::cout << "Peek: " << tokenName(m_currentToken.type) << std::endl;
		return m_currentToken;
	}

  inline Token PeekNextToken() const
  {
    return m_nextToken;
  }

  inline bool Match(TokenType expected)
  {
    return (PeekToken().type == expected);
  }

  inline bool MatchNext(TokenType expected)
  {
    return (PeekNextToken().type == expected);
  }
};
